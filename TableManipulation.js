$(document).ready(function() {
    $("#addButton").on("click", addRow);
    $("#resetButton").on("click", resetRows);
    $("th").addClass("px-1");
    window.addEventListener("beforeunload", confirmExit)
});

function addRow(event) {
    var target = $("#tableBody");
    var rowNum = target.children().length;
    var tr = $("<tr></tr>").addClass("d-flex");
    target.append(tr)
    var td = $("<td></td>").addClass("col-1");
    var name = $("<input>").addClass("form-control font-weight-bold name-" + rowNum).attr("type", "text").attr("id", "name-" + rowNum).attr("data-loc", rowNum + "-" + (0));
    td.append(name);
    tr.append(td);
    //Create the remainder of the row
    for (i = 12; i >= 0; --i) {
        td = $("<td></td>").addClass("col");
        var input = $("<input>").addClass("form-control font-weight-bold score mx-auto name-" + rowNum).attr("type", "text").attr("id", i + "-dots-row-" + rowNum).attr("data-loc", rowNum + "-" + (13 - i));
        input.val(0).attr("data-dots", i + "-dots");
        td.append(input);
        tr.append(td)
    }
    //Add the score
    td = $("<td></td>").addClass("col");
    var total = $("<input disabled>").addClass("form-control font-weight-bold total mx-auto name-" + rowNum).attr("type", "text").attr("id", "score-" + rowNum).attr("data-loc", rowNum + "-" + (15));
    total.val(0);
    td.append(total);
    tr.append(td);
    $("input").on("keydown", changeFocus)
    $("input.score").on("change", updateTotal)
    $("input.score").focus(function () {
        $(this).select().one('mouseup', function (e) {
            $(this).off('keyup');
            e.preventDefault();
        }).one('keyup', function () {
            $(this).select().off('mouseup');
        });
    });
    name.focus();
    $("td").addClass("px-1");
}

function resetRows(event) {
    var values = $("table").find(".score");
    values.each(function() {
        $(this).val(0);
    });

    var totals = $("table").find(".total");
    totals.each(function() {
        $(this).val(0);
    });

    $(".finished").removeClass("finished");
}

function updateTotal() {
    var parent = $(this).parent().parent();
    var values = parent.find(".score");
    var total = parent.find(".total");
    total.val(0);
    values.each(function() {
        addToTotal(total, $(this));
    });
    checkCompletedCol($(this));
}

function addToTotal(total, current) {
    var original = parseFloat(total.val());
    var toAdd = parseFloat(current.val());
    if (toAdd < 0 || isNaN(toAdd)) {
        toAdd = 0;
        current.addClass("invalid-input");
    }
    else {
        if (current.hasClass("invalid-input")) {
            current.removeClass("invalid-input");
        }
    }
    total.val(original + toAdd);
}

function changeFocus(event) {
    var start = new Date().getTime();
    var target = $(event.target)
    var data = $(event.target).attr("data-loc");
    var index = data.indexOf("-");
    var row = parseInt(data.substring(0, index));
    var col = parseInt(data.substr(index + 1));
    //38 == up, 40 == down, 37 == left, 39 == right
    if (event.which == 38) {
        if (row > 0) {
            $("[data-loc=" + (row - 1) +  "-" + col + "]").focus();
        }
    }
    else if (event.which == 40) {
        var max = $("#tableBody").children().length;
        if (row < max) {
            $("[data-loc=" + (row + 1) + "-" + col + "]").select();
        }
    }
    else if (event.which == 37) {
        if (col > 0) {
            $("[data-loc=" + row +  "-" + (col - 1) + "]").select();
        }
    }
    else if (event.which == 39) {
        if (col < 14) {
            $("[data-loc=" + row +  "-" + (col + 1) + "]").focus();
        }
    }
}

function checkCompletedCol(current) {
    var dots = current.attr("data-dots");
    var all = $("[data-dots=" + dots + "]")
    var numZeros = 0
    for (i = 0; i < all.length; ++i) {
        if ($(all[i]).val() == 0) {
            ++numZeros;
        }
    }
    if (numZeros <= 1) {
        all.addClass("finished");
    }
    else {
        all.removeClass("finished");
    }
}

function confirmExit(event) {
    event.preventDefault();
    event.returnValue = '';
}